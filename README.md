# Curso de Programación Orientado a Objetos

En este curso emplearemos emplearemos el lenguaje [Python](https://www.python.org/) y como herramienta de desarrollo el entorno [Jupyter Notebook](https://jupyter.org/)

# Tabla de contenidos 💡

## I. Conceptos del paradigma de la Programación Orientada a Objetos

- [1.1 Preparando el ambiente de trabajo](scripts/1.1.preparando_ambiente_trabajo.md)
- [1.2 Conceptos fundamentales](scripts/1.2.conceptos_fundamentales.md)
- [1.3 Clases y objetos](scripts/1.3.clases_objetos.md)
- [1.4 Encapsulamiento o abstracción de atributos](scripts/1.4.encapsulamiento_abstraccion_atributos.md)
- [1.5 Métodos de acceso](scripts/1.5.metodos_acceso.md)
- [1.6 Constructores y destructores](scripts/1.6.constructores_destructores.md)
- [1.7 Sobrecarga de operaciones](scripts/1.7.sobrecarga_operaciones.md)

## II. Encapsulamiento y ocultamiento de información

- [2.1 Aplicación de Encapsulación](scripts/2.1.aplicacion_encapsulacion.md)
- [2.2 Modularidad (criterios, principios y reglas)](scripts/2.2.modularidad.md)
- [2.3 El concepto de interfaz](scripts/2.3.interfaz.md)
- [2.4 Uso de las Metaclases](scripts/2.4.metaclases.md)
- [2.5 Relaciones entre clases](scripts/2.5.relaciones_clases.md)

## III. Herencias y composición

- [3.1 Introducción a la herencia](scripts/3.1.introduccion_herencia.md)
- [3.2 Herencia simple](scripts/3.2.herencia_simple.md)
- [3.3 Herencia múltiple](scripts/3.3.herencia_multiple.md)
- [3.4 Herencia de interfaz](scripts/3.4.herencia_interfaz.md)
- [3.5 Herencia de implementación](scripts/3.5.herencia_implementacion.md)
- [3.6 Elección de la técnica de reutilización](scripts/3.6.eleccion_tecnica_reutilizacion%5D.md)

## IV .Polimorfismo

- [4.1 Definición de Polimorfismo](scripts/4.1.definicion_polimorfismo.md)
- [4.2 Concepto de polimorfismo por herencia](scripts/4.2.concepto_polimorfismo_herencia.md)
- [4.3 Polimorfismo](scripts/4.3.polimorfismo.md)
- [4.4 Polimorfismo en jerarquías de herencia](scripts/4.4.polimorfismo_jerarquias_herencia.md)
- [4.5 Variables Polimórficas](scripts/4.5.variables_polimorficas.md)


## V Presentaciones del curso

- [5.1 Presentaciones](https://drive.google.com/drive/folders/1RJmK5Yxj_4reUMfhA8owc_TdJclnWQn3)

# Materiales de consulta

* [Git](#git)
* [Python](#python)

## Git

Nombre | Descripción | Tipo | Idioma | Gratuito | Link |
|---|---|---|---|---|---|
| Documentación Oficial de Git | Documentación Oficial de Git | Documentación | Inglés | Sí | [Link](https://www.git-scm.com/doc) |
| Git Immersión en Español | Git immersión es un tour guiado que lo lleva por los fundamentos de git | Immersión | Español | Sí | [Link](https://esparta.github.io/gitimmersion-spanish/) |
| Los Apuntes de Majo(GIT) | Cheat Sheet sobre los comandos básicos de GIT y workflows | Cheat Sheet | Español | Sí | [Link](https://drive.google.com/file/d/1sHgKrrea1-HpityOEYqFLjRdaum85CnW/view) |
| Git. La guía simple | Una guía sencilla para comenzar con git sin complicaciones | Cheat Sheet | Español | Sí | [Link](https://rogerdudler.github.io/git-guide/index.es.html) |
| Gitmagic | Guía para comprender el control de versiones de Git. | Libro | Español | Sí | [Link](http://www-cs-students.stanford.edu/~blynn/gitmagic/intl/es/) |
| Pro Git | El libro completo de Pro Git, escrito por Scott Chacon y Ben Straub | Libro | Español | Sí | [Link](https://git-scm.com/book/es/v2) |
| Git Cheat Sheets | Hojas de referencia que cubren los comandos Git, sus características, las migraciones SVN y bash| Libro | Disponible en varios idiomas | Sí | [Link](https://training.github.com/) |

**[⬆ Volver al índice](#índice)**

## Python

|Nombre | Descripción | Tipo | Idioma | Gratuito | Link |
|---|---|---|---|---|---|
| 🐍 El Libro de Python 🐍 | Libro Online | Libro | Español | Sí | [Link](https://ellibrodepython.com/) |
| Aprende python desde cero. Primeros pasos. |  De programador para programadores: empieza en el mundo de Python con este curso 
| Aprende a Programar en Python Desde Cero |  Curso Completo Gratis de 4.5+ Horas | Curso online | Español | Sí | [Link](https://youtu.be/DLikpfc64cA) |
| Curso de Python científico en español | De cero a las bases de Python científico | Tutorial | Español | Sí | [Link](https://github.com/brivadeneira/curso-python-cientifico-ing-unrc) |
| Curso de Python de propósito general |  De cero a las bases de Python | Tutorial | Español | Sí | [Link](https://github.com/brivadeneira/curso-python-rio4) |
| Curso de Python desde cero | Lista de reproducción para aprender Python | Tutorial | Español | Sí | [Link](https://www.youtube.com/watch?v=G2FCfQj-9ig&list=PLU8oAlHdN5BlvPxziopYZRd55pdqFwkeS&ab_channel=pildorasinformaticas) |
| Curso Python para Principiantes | Tutorial de Python desde cero | Tutorial | Español | Sí | [Link](https://www.youtube.com/watch?v=chPhlsHoEPo&ab_channel=Fazt) |
| Intro a programación con Python | Tutorial de Python | Repositorio | Español | Sí | [Link](https://github.com/RodolfoFerro/python-innovaccion) |
| Python Docs | Documentación oficial de Python | Documentación | Español | Sí | [Link](https://docs.python.org/es/3/) |
| Python para principiantes 🐍 Curso GRATIS desde cero | Desde Afi Escuela de Finanzas ponemos a disposición el curso gratuito de Python impartido por Javier Calderón, Consultor del Área de Finanzas Cuantitativas de Afi.  | Video Tutorial | Español | Sí | [Link](https://www.youtube.com/watch?v=BrJE-4euNn0) |
| Tutorial de Python en español | Tutorial oficial de Python | Tutorial | Español | Sí | [Link](https://tutorial.python.org.ar/en/latest/) |

**[⬆ Volver al índice](#índice)**
